<?php
function add_child_template_support() {
	 /*add theme colors*/
	add_theme_support(
		'editor-color-palette',
		array(
			array(
				'name'  => __( 'Primary', 'wp-theme-parent' ),
				'slug'  => 'primary',
				'color' => '#2d3258',
			),
			array(
				'name'  => __( 'Secondary', 'wp-theme-parent' ),
				'slug'  => 'secondary',
				'color' => '#29bab3',
			),
			array(
				'name'  => __( 'White', 'wp-theme-parent' ),
				'slug'  => 'white',
				'color' => '#ffffff',
			),
			array(
				'name'  => __( 'Dark', 'wp-theme-parent' ),
				'slug'  => 'dark',
				'color' => '#333333',
			),
		)
	);

	// add support for editor styles
	add_theme_support( 'editor-styles' );

	add_theme_support( 'align-wide' );
}

add_action( 'after_setup_theme', 'add_child_template_support', 20 );
