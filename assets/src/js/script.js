import "../sass/style.scss";
require('./window-event/ready.js');
require('./window-event/load.js');
require('./window-event/resize.js');
require('./window-event/scroll.js');
require('./gallery.js');
