// Windows Ready Handler
(function($) {

    $(document).ready(function(){
      $("#site-header .nav-link").click(function (){
        $('[data-toggle="offcanvas"]').trigger("click");
      });
    });

}(jQuery));
