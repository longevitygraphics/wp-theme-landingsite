(function ($) {
  window.wpBlockGallerySwiper = null;
  $(document).ready(function () {
    $(".wp-block-gallery a").click(function (e) {
      e.preventDefault();
    });
    $(".wp-block-gallery img").click(function () {

      //setup swiper if not existing
      if (window.wpBlockGallerySwiper == null) {
        const swiperHtml = '<div class="swiper-container swiper1"><div class="swiper-wrapper"></div><div class="swiper-pagination swiper-pagination-white"></div><div class="swiper-button-next swiper-button-white"></div><div class="swiper-button-prev swiper-button-white"></div></div>';
        $("body").append(swiperHtml);

        window.wpBlockGallerySwiper = new Swiper('.swiper1', {
          preloadImages: false,
          // Enable lazy loading
          lazy: true,
          pagination: {
            el: '.swiper-pagination',
            clickable: true,
          },
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
          watchSlidesVisibility: true
        });
      }else{
        window.wpBlockGallerySwiper.removeAllSlides();
      }

      //prepare slides from gallery
      const wpBlockGalleryEl = $(this).parents(".wp-block-gallery");
      let wpBlockGallerySlides = [];
      wpBlockGalleryEl.find('img').each(function (index) {
        wpBlockGallerySlides.push('<div class="swiper-slide"> <img data-srcset="' + $(this).attr('srcset') + '" data-sizes="' + $(this).attr('sizes') + '" data-src="' + $(this).attr('src') + '" class="swiper-lazy"/><div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div></div>');
      });


      window.wpBlockGallerySwiper.addSlide(1, wpBlockGallerySlides);
      window.wpBlockGallerySwiper.lazy.load();
      //window.wpBlockGallerySwiper.update();
    });


  });

}(jQuery));
